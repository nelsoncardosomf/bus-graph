# Bus Graph

Repositório destinado ao primeiro projeto da disciplina de Teoria dos Grafos no semestre 2019.1

Tecnologias Utilizadas:
* Javascript
* Node.js
* HTML5
* CSS3

Bibliotecas Utilizadas:
* Express.js
* Body-parser
* Mustache


Para instalar Node.js execute os seguinte comandos:
```bash 
	sudo apt install nodejs npm
	npm install 
	npm install express 
	npm install body-parser
	npm install mustache
```
