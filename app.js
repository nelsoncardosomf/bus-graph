const express = require('express')
const engines = require('consolidate')
const app = express()
const lineReader = require('line-reader')
const bodyParser = require('body-parser')

app.use(bodyParser.json()) // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
app.use(express.static('public'))
app.engine('html', engines.mustache)
app.set('view engine', 'html')


app.get('/',function(req,res){
    lineReader.eachLine('./public/file/bus-network.txt',function(line,last){
        console.log("Foda-se ", line)
    })
});

app.listen(8000);